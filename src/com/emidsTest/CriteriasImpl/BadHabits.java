/**
 * 
 */
package com.emidsTest.CriteriasImpl;

import com.emidsTest.Criterias.Habbits;

/**
 * @author emidstest03
 *
 */
public class BadHabits implements Habbits{
	public static final String ALL_BAD_HABITS = "smoking drug alcohol ";

	public String getHabitName() {
		
		return ALL_BAD_HABITS;
	}
}
