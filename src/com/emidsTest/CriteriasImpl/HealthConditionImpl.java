/**
 * 
 */
package com.emidsTest.CriteriasImpl;

import com.emidsTest.Criterias.HealthConditions;

/**
 * @author emidstest03
 *
 */
public class HealthConditionImpl implements HealthConditions{

	String healthIssue;
	
	public String getHealthCondition() {
		
		return healthIssue;
	}
	
	
	public HealthConditionImpl(String n){
		this.healthIssue = n;
	}
	
	public boolean equals(HealthConditionImpl h){
		if(h.getHealthCondition().equals(this.healthIssue)){
			return true;
		}
		return false;
	}
	
	public int hashCode(HealthConditionImpl h){
		return h.getHealthCondition().hashCode();
	}

}
