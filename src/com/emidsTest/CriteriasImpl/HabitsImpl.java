/**
 * 
 */
package com.emidsTest.CriteriasImpl;

import com.emidsTest.Criterias.Habbits;

/**
 * @author emidstest03
 *
 */
public class HabitsImpl implements Habbits{
	String hName;

	public String getHabitName() {
		
		return hName;
	}
	
	public HabitsImpl(String n){
		this.hName = n;
	}
	
	public boolean equals(HabitsImpl h){
		if(h.getHabitName().equals(this.hName)){
			return true;
		}
		return false;
	}
	
	public int hashCode(HabitsImpl h){
		return h.getHabitName().hashCode();
	}

}
