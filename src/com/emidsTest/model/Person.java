/**
 * 
 */
package com.emidsTest.model;

import com.emidsTest.Criterias.HealthConditions;
import com.emidsTest.Criterias.Habbits;

import java.util.List;
import java.util.Set;

/**
 * @author emidstest03
 *
 */
public class Person {
	
	
	private String name;
	private int age;
	private String gender;
	private Set<HealthConditions> healthConditions;
	private Set<Habbits> habbitsList;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Set<HealthConditions> getHealthConditions() {
		return healthConditions;
	}
	public void setHealthConditions(Set<HealthConditions> healthConditions) {
		this.healthConditions = healthConditions;
	}
	public Set<Habbits> getHabbitsList() {
		return habbitsList;
	}
	public void setHabbitsList(Set<Habbits> habbitsList) {
		this.habbitsList = habbitsList;
	}
	
	
	
	
	

}
