package com.emidsTest.test;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.emidsTest.Calculator.HealthPremium;
import com.emidsTest.Calculator.HealthPremiumCalculator;
import com.emidsTest.Criterias.Habbits;
import com.emidsTest.Criterias.HealthConditions;
import com.emidsTest.CriteriasImpl.HabitsImpl;
import com.emidsTest.CriteriasImpl.HealthConditionImpl;
import com.emidsTest.model.Person;

public class HealthTest {
	HealthPremium hpc = new HealthPremium();
	double premium = 0.0;
	
	Person per = new Person();

	@Test
	public void testWithoutHealth() {
		System.out.println("TEST without health issues list");
		per.setName("Norman Gomes");
		double premiumValue = 5000f;
		per.setHealthConditions(null);
		premiumValue = hpc.getPremium(premium, per);
		assertEquals((double) 5000, premiumValue, 0.0f);
	}
	
	@Test
	public void testCurrentHealthPremium() {
		System.out.println("TEST with health issues list ***");
		per = intialisePerson();
		double premiumValue = hpc.getPremium(premium, per);
		assertEquals((double) 5050, premiumValue, 0.0f);
	}


	
	
	private Person intialisePerson(){
		// initialise person
				 /*Set<Habbits> hList = new HashSet<Habbits>();
		HabitsImpl h = new HabitsImpl("smoking");
		HabitsImpl h1 = new HabitsImpl("exercise");
		hList.add(h);
		hList.add(h1);*/
		
				Set<HealthConditions> healthConditions =  new HashSet<HealthConditions>();
				HealthConditionImpl hc = new HealthConditionImpl("overweight");
				healthConditions.add(hc);
				
				this.per.setName("Mr Gomes");
				// this.per.setAge(34);
				this.per.setGender("male");
				// this.per.setHabbitsList(hList);
				this.per.setHealthConditions(healthConditions);
				
		return this.per;
	}
}
