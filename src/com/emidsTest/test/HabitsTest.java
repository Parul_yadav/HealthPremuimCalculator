package com.emidsTest.test;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.emidsTest.Calculator.HabitPremium;
import com.emidsTest.Calculator.HealthPremiumCalculator;
import com.emidsTest.Criterias.Habbits;
import com.emidsTest.Criterias.HealthConditions;
import com.emidsTest.CriteriasImpl.HabitsImpl;
import com.emidsTest.CriteriasImpl.HealthConditionImpl;
import com.emidsTest.model.Person;

public class HabitsTest {
	HabitPremium hpc = new HabitPremium();
	double premium = 0.0;
	
	Person per = new Person();

	@Test
	public void testWithoutHabits() {
		System.out.println(" Test testWithoutHabits " );
		per.setName("Norman Gomes");
		double premiumValue = 5000;
		per.setHabbitsList(null);
		premiumValue = hpc.getPremium(premium, per);
		assertEquals((double) 5000, premiumValue, 0.0);
	}
	
	@Test
	public void testCurrentHabitPremium() {
		System.out.println(" Test with Habits " );
		per = intialisePerson();
		double premiumValue = hpc.getPremium(premium, per);
		assertEquals((double) 4995.5, premiumValue, 0.0);
	}


	
	
	private Person intialisePerson(){
		// initialise person
				Set<Habbits> hList = new HashSet<Habbits>();
				// Set<HealthConditions> healthConditions =  new HashSet<HealthConditions>();
				HabitsImpl h = new HabitsImpl("smoking");
				HabitsImpl h1 = new HabitsImpl("exercise");
				hList.add(h);
				hList.add(h1);
				
				
				this.per.setName("Mr Gomes");
				//this.per.setAge(34);
				this.per.setGender("male");
				this.per.setHabbitsList(hList);
				// this.per.setHealthConditions(healthConditions);
				
		return this.per;
	}
}
