package com.emidsTest.test;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;

import org.junit.Test;

import org.junit.runner.JUnitCore;


import static org.junit.Assert.*;

import com.emidsTest.Calculator.HealthPremiumCalculator;
import com.emidsTest.Criterias.Habbits;
import com.emidsTest.Criterias.HealthConditions;
import com.emidsTest.CriteriasImpl.HabitsImpl;
import com.emidsTest.CriteriasImpl.HealthConditionImpl;
import com.emidsTest.Utils.Constants;
import com.emidsTest.model.Person;


public class PremiumCalculatorTest {
	Set<Habbits> hList = new HashSet<Habbits>();
	Set<HealthConditions> healthConditions =  new HashSet<HealthConditions>();
	HealthPremiumCalculator hpc = new HealthPremiumCalculator();
	
	Person person = new Person();
	double premium=5000;
	
	@Before
	public void intialisePerson(){
		
		HabitsImpl h = new HabitsImpl("smoking");
		HabitsImpl h1 = new HabitsImpl("exercise");
		hList.add(h);
		hList.add(h1);
		
		HealthConditionImpl hc = new HealthConditionImpl("overweight");
		healthConditions.add(hc);
		
		this.person.setName("Mr Gomes");
		this.person.setAge(34);
		this.person.setGender("male");
		this.person.setHabbitsList(hList);
		this.person.setHealthConditions(healthConditions);
		
	}
	
	@Test
	public void test() {
		System.out.println("");
		System.out.println("--- CALculating TOTAL PRemium --- ");
		
		
		premium = hpc.getPremium(premium, this.person);
		//assertEquals((double) 6690.2734, premium, 0.0);
		
		System.out.println(" ");
		System.out.println("--->>>>  TOTAL Premium = "+premium);
		
	}
	
	

}
