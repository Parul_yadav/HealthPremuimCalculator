package com.emidsTest.test;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.emidsTest.Calculator.GenderPremium;
import com.emidsTest.Calculator.HealthPremiumCalculator;
import com.emidsTest.Criterias.Habbits;
import com.emidsTest.Criterias.HealthConditions;
import com.emidsTest.CriteriasImpl.HabitsImpl;
import com.emidsTest.CriteriasImpl.HealthConditionImpl;
import com.emidsTest.model.Person;

public class GenderTest {
	GenderPremium hpc = new GenderPremium();
	double premium = 0.0;
	
	Person per = new Person();
	
	
	@Test
	public void testA_Without() {
		System.out.println("testWithoutPerson");
		double premiumValue = 5000f;
		per.setName("Mr. Gomes");
		premiumValue = hpc.getPremium(premium, per);
		assertEquals((double) 5000, premiumValue, 0.0f);
	}
	
	@Test
	public void testB_GenderPremium() {
		System.out.println("testAgePremium");
		per = intialisePerson();
		
		double premiumValue = 0.0f;
		premiumValue = hpc.getPremium(premium, per);
		assertEquals((double) 5050, premiumValue, 0.0f);
	}
	
	private Person intialisePerson(){
		// initialise person
				/*Set<Habbits> hList = new HashSet<Habbits>();
				Set<HealthConditions> healthConditions =  new HashSet<HealthConditions>();
				HabitsImpl h = new HabitsImpl("smoking");
				HabitsImpl h1 = new HabitsImpl("exercise");
				hList.add(h);
				hList.add(h1);
				
				HealthConditionImpl hc = new HealthConditionImpl("overweight");
				healthConditions.add(hc);
				*/
				this.per.setName("Mr Gomes");
				//this.per.setAge(34);
				this.per.setGender("male");
				//this.per.setHabbitsList(hList);
				//this.per.setHealthConditions(healthConditions);
				
		return this.per;
	}

}
