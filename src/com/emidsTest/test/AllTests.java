package com.emidsTest.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AgeTest.class, GenderTest.class, HealthTest.class, HabitsTest.class, PremiumCalculatorTest.class })
public class AllTests {

}
