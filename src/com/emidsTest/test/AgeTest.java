/**
 * 
 */
package com.emidsTest.test;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;


import org.junit.Test;

import com.emidsTest.Calculator.AgePremium;
import com.emidsTest.Calculator.HealthPremiumCalculator;
import com.emidsTest.Criterias.Habbits;
import com.emidsTest.Criterias.HealthConditions;
import com.emidsTest.CriteriasImpl.HabitsImpl;
import com.emidsTest.CriteriasImpl.HealthConditionImpl;
import com.emidsTest.model.Person;

public class AgeTest {
	double premium = 0.0;
	
	AgePremium hpc = new AgePremium();
	
	Person person = new Person();
	
	
	@Test
	public void testA_WithoutPerson() {
		System.out.println("testWithoutPerson");
		double premiumValue = hpc.getPremium(premium, person);
		assertEquals((double) 0.0, premiumValue, 0.0f);
	}
	
	@Test
	public void testB_WithoutAge() {
		System.out.println("testAgePremium");
		this.person.setName("Mr Gomes");
		
		double premiumValue = 0.0f;
		premiumValue = hpc.getPremium(premium, person);
		assertEquals((double) 5000, premiumValue, 0.0f);
	}
	
	@Test
	public void testB_AgePremium() {
		System.out.println("testAgePremium");
		person = intialisePersonAge();
		
		double premiumValue = 0.0f;
		premiumValue = hpc.getPremium(premium, person);
		assertEquals((double) 6630, premiumValue, 0.0f);
	}
	
	private Person intialisePersonAge(){
		
				this.person.setName("Mr Gomes");
				this.person.setAge(34);
				
				
		return this.person;
	}

}
