/**
 * 
 */
package com.emidsTest.test;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * @author emidstest03
 *
 */
public class TestExecutor {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Result r = JUnitCore.runClasses(AllTests.class);
		
		for(Failure f : r.getFailures())
		{
			f.getMessage();
		}
	}

}
