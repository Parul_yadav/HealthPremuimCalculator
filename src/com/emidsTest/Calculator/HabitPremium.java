/**
 * 
 */
package com.emidsTest.Calculator;

import java.util.Set;

import com.emidsTest.Criterias.Habbits;
import com.emidsTest.CriteriasImpl.BadHabits;
import com.emidsTest.CriteriasImpl.GoodHabits;
import com.emidsTest.Utils.Constants;
import com.emidsTest.model.Person;

/**
 * @author emidstest03
 *
 */
public class HabitPremium implements PremiumCalculator {
	double premiumRate = 0.0;
	double basePremium = 5000;
	double premium = 0.0;

	/* (non-Javadoc)
	 * @see com.emidsTest.Calculator.PremiumCalculator#getPrimiumRate(com.emidsTest.model.Person)
	 */
	public double getPrimiumRate(Person person) {
		if(null != person) {
			if(null!=person.getHabbitsList() && !person.getHabbitsList().isEmpty()){
				premiumRate = 0.03;
			}else {
				premiumRate = 0.0;
			}
		}
		
     System.out.println("premium rate in habits cri " + premiumRate);
    return premiumRate;
	}

	/* (non-Javadoc)
	 * @see com.emidsTest.Calculator.PremiumCalculator#getPremium(double, double, com.emidsTest.model.Person)
	 */
	public double getPremium(double premium, Person person) {
		System.out.println("--->>> habit initialPremium  "+premium);
		// premium = basePremium;
		if(null != person) {
			double premiumRate = getPrimiumRate(person);
			
			if(null!=person.getHabbitsList() && !person.getHabbitsList().isEmpty()){
				Set<Habbits> habbitsList = person.getHabbitsList();
			    
				// Increase 3% per bad habbit
			    for (Habbits habbit : habbitsList) {
			    	
			        if (BadHabits.ALL_BAD_HABITS.contains(habbit.getHabitName())) {
			        	
			        	premium= premium + (basePremium * premiumRate);
			        	System.out.println(" Bad Habit : -3% , Premium : " +premium);
			        }
			    }
			    // Decrease 3% per good habbit
			    for (Habbits habbit : habbitsList) {
			        if (GoodHabits.ALL_GOOD_HABITS.contains(habbit.getHabitName())) {
			        	
			        	premium = premium - (basePremium * premiumRate);
			        	System.out.println(" Good Habit : +3% , Premium : " +premium);
			        }
			    }
			}
	}
     System.out.println("premium in habits cri " + premium);
    return premium;

}
}
