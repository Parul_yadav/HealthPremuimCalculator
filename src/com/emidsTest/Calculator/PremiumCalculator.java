package com.emidsTest.Calculator;


import com.emidsTest.model.Person;

public interface PremiumCalculator {
	
	public double getPrimiumRate(Person person);
	 public double getPremium(double premium, Person p);
	
	

}
