/**
 * 
 */
package com.emidsTest.Calculator;

import com.emidsTest.model.Person;

/**
 * @author emidstest03
 *
 */
public class AgePremium implements PremiumCalculator {
	double premiumRate = 0.0;
	double basePremium = 5000;
	double premium = 0.0;

	
	public double getPrimiumRate(Person person) {
		if(null != person) {
			if (person.getAge() < 18) {
				premiumRate = 0.0;
			} else if (person.getAge() >= 40) {
				premiumRate = 0.2; 
			}else{
				premiumRate = 0.1; 
			}
		}
		System.out.println("Age Premium Rate: " + premiumRate);

	    return premiumRate;
	}

	public double getPremium(double premium, Person person) {
		System.out.println("--->>> age initialPremium  "+premium);
		if(null != person) {
			
			double premiumRate = getPrimiumRate(person);
			if (person.getAge() < 18) {
				premium = (premium + (basePremium * premiumRate));
			}
			if (person.getAge() >= 18 && person.getAge() < 25) {
				premium = (premium + (basePremium * premiumRate));
			} else if (person.getAge() >= 25 && person.getAge() < 30) {
				premium = (premium + (basePremium * premiumRate) * 2);
			} else if (person.getAge() >= 30 && person.getAge() < 35) {
				premium =  (premium + (basePremium * premiumRate) * 3);
			} else if (person.getAge() >= 35 && person.getAge() < 40) {
				premium =  (premium + (basePremium * premiumRate) * 4);
			} else if (person.getAge() >= 40) {
				
				premium =  (premium + (basePremium * 0.1) * 4 + (basePremium * premiumRate));
			}
		}
		
		System.out.println("Age Premium : Rs. " + premium);
		
		return premium;
	}


}
