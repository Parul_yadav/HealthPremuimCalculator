/**
 * 
 */
package com.emidsTest.Calculator;

import java.util.Set;

import com.emidsTest.Criterias.Habbits;
import com.emidsTest.Criterias.HealthConditions;
import com.emidsTest.CriteriasImpl.BadHabits;
import com.emidsTest.CriteriasImpl.GoodHabits;
import com.emidsTest.model.Person;

/**
 * @author emidstest03
 *
 */
public class HealthPremium implements PremiumCalculator {


	double premiumRate = 0.0;
	double basePremium = 5000;
	double premium = 0.0;

	/* (non-Javadoc)
	 * @see com.emidsTest.Calculator.PremiumCalculator#getPrimiumRate(com.emidsTest.model.Person)
	 */
	public double getPrimiumRate(Person person) {
		if(null != person) {
			if(null!=person.getHealthConditions() && !person.getHealthConditions().isEmpty()){
					premiumRate=  0.01;
			}else{
				premiumRate = 0.0;
			}
		}
		 
     System.out.println("premium rate in health cri " + premiumRate);
    return premiumRate;
	}

	/* (non-Javadoc)
	 * @see com.emidsTest.Calculator.PremiumCalculator#getPremium(double, double, com.emidsTest.model.Person)
	 */
	public double getPremium(double premium, Person person) {
		System.out.println("--->>> health initialPremium  "+premium);
		// premium = basePremium;
		double premiumRate= getPrimiumRate(person);
		if(null != person) {
			if(null!=person.getHealthConditions() && !person.getHealthConditions().isEmpty()){
				for (HealthConditions issue : person.getHealthConditions()) {
					premium= premium + (basePremium * premiumRate);
			    }
			}
		}
		 System.out.println("premium in health cri " + premium);
		return premium;

}

}
