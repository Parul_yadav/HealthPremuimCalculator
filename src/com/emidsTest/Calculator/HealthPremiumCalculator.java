/**
 * 
 */
package com.emidsTest.Calculator;

import java.util.Set;

import com.emidsTest.Criterias.Habbits;
import com.emidsTest.Criterias.HealthConditions;
import com.emidsTest.CriteriasImpl.BadHabits;
import com.emidsTest.CriteriasImpl.GoodHabits;
import com.emidsTest.Utils.Constants;
import com.emidsTest.model.Person;

/**
 * @author emidstest03
 *
 */
public class HealthPremiumCalculator implements PremiumCalculator{
	
	private double basePremium = 5000; // base premium
	private double premium = 0.0;
	
	private double premiumRate = 0;
	
	


public double getPrimiumRate(Person p) {	
	if(null != p) {
		premiumRate = getPrimiumRate(p, "age");
		premiumRate = getPrimiumRate(p, "gender");
		premiumRate = getPrimiumRate(p, "habit");
		premiumRate = getPrimiumRate(p, "health");
	}
	return premiumRate;
	}

public double getPrimiumRate(Person p, String type) {
	if(null != p) {
		if(type.equals("age")){
			return new AgePremium().getPrimiumRate(p) ;
		}
		if(type.equals("gender")){
			return new GenderPremium().getPrimiumRate(p);
		}
		if(type.equals("health")){
			return new HealthPremium().getPrimiumRate(p); 
			
		}
		if(type.equals("habit")){
			return new HabitPremium().getPrimiumRate(p); 
			
		}
		
	}
	return premiumRate;
	}


	public double getPremium(double premium, Person p) {
		double totalPremium =premium;
		if(null != p) {
			
			totalPremium = new GenderPremium().getPremium(totalPremium, p);
			System.out.println("--->>> Gender totalPremium  "+totalPremium);
			System.out.println("");
			
			
			totalPremium = new AgePremium().getPremium(totalPremium, p) ;
			System.out.println("--->>> age totalPremium " +totalPremium);
			System.out.println("");
			
			
			totalPremium =  new HealthPremium().getPremium(totalPremium, p); 
			System.out.println("--->>> health totalPremium "+totalPremium);
			System.out.println("");
			
			
			totalPremium = new HabitPremium().getPremium(totalPremium, p); 
			System.out.println("--->>> habit totalPremium "+totalPremium);
			System.out.println("");
		}
		return totalPremium;
	}

	
}
