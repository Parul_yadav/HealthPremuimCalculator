/**
 * 
 */
package com.emidsTest.Calculator;

import com.emidsTest.Utils.Constants;
import com.emidsTest.model.Person;

/**
 * @author emidstest03
 *
 */
public class GenderPremium implements PremiumCalculator {
	double premiumRate = 0.0;
	double basePremium = 5000;
	double premium = 0.0;

	/* (non-Javadoc)
	 * @see com.emidsTest.Calculator.PremiumCalculator#getPrimiumRate(com.emidsTest.model.Person)
	 */
	public double getPrimiumRate(Person p) {
		if(null != p) {
			
			if (null != p.getGender() && p.getGender().equals(Constants.MALE)) {
				premiumRate = 0.00;
			}else if (null != p.getGender() && p.getGender().equals(Constants.FEMALE)){
				premiumRate = 0.02;
			}else if(null != p.getGender() && p.getGender().equals(Constants.OTHERS)){
				premiumRate = 0.04;
			
			
			}
		}
		 System.out.println("premium rate in gender cri " + premiumRate);

		return premiumRate;
	}

	/* (non-Javadoc)
	 * @see com.emidsTest.Calculator.PremiumCalculator#getPremium(double, double, com.emidsTest.model.Person)
	 */
	public double getPremium(double premium, Person p) {
		System.out.println("--->>> Gender initialPremium  "+premium);
		if(null != p) {
			
			double premiumRate = getPrimiumRate(p);
			
			if (null != p.getGender()) {
			premium= premium + (basePremium * premiumRate);
			}
		}
		 System.out.println("premium in gender cri " + premium);

		return premium;
	}

}
